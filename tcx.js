let tcx = module.exports = {};

let position = (lat, lon) => {
    if (lat === undefined || lon === undefined) return '<Position/>';

    return '<Position>' +
                `<LatitudeDegrees>${lat}</LatitudeDegrees>` +
                `<LongitudeDegrees>${lon}</LongitudeDegrees>` +
            '</Position>';
};

let elevation = ele => {
    return ele === '' ? '<AltitudeMeters/>' : `<AltitudeMeters>${ele}</AltitudeMeters>`;
};

tcx.mk = o => {
    let trackpoints = '';
    o.points.forEach((v, k) => {
        trackpoints += '<Trackpoint>' +
                `<Time>${v.time}</Time>` +
                `<DistanceMeters>${v.distance}</DistanceMeters>` +
                position(v.lat, v.lon) +
                elevation(v.elevation) +
            '</Trackpoint>';
    });

    return '<?xml version="1.0" encoding="UTF-8"?>' +
        '<TrainingCenterDatabase' +
            ' xmlns="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2"' +
            ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' +
            ' xsi:schemaLocation="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2' +
                ' http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd">' +
            '<Activities>' +
                `<Activity Sport="${o.sport}">` +
                    `<Id>${o.id}</Id>` +
                    `<Lap StartTime="${o.start_time}">` +
                        `<TotalTimeSeconds>${o.total_time}</TotalTimeSeconds>` +
                        `<DistanceMeters>${o.distance}</DistanceMeters>` +
                        '<Intensity>Active</Intensity>' +
                        '<TriggerMethod>Manual</TriggerMethod>' +
                        '<Track>' +
                            trackpoints +
                        '</Track>' +
                    '</Lap>' +
                '</Activity>' +
            '</Activities>' +
        '</TrainingCenterDatabase>';
};
