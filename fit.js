#!/bin/node

const fs = require('fs'),
    EasyFit = require('easy-fit').default
;

let fit = {}; module.exports = fit;

fit.parse = async o => {
    let ef = new EasyFit(o.opt);

    return new Promise((resolve, reject) => {
        ef.parse(fs.readFileSync(o.f), (err, d) => {
            if (err) reject(err);
            if (d.activity.num_sessions !== 1) reject('err: more than one session');
            resolve(d);
        });
    });
};

