const path = require('path'),
    tcx = require('./tcx'),
    fit = require('./fit'),
    elevation = require('elevation')
;

let conv = module.exports = {};

conv.fit_tcx = async o => {
    let ele = elevation(o.geotiff);

    let id = path.basename(o.fit, '.fit');

    let sport = { 0: 'Running', 2: 'Biking' };

    let fit_data;

    try {
        fit_data = await fit.parse({
            f: o.fit,
            opt: {
                speedUnit: 'm/s',
                lengthUnit: 'm',
                temperatureUnit: 'celsius',
                elapsedRecordField: true,
                mode: 'cascade'
            }
        });
    } catch(err) {
        return { err: [ 'unable to parse: '+o.fit, err ] };
    }

    let param = {
        sport: (o.activity_type in sport ? sport[o.activity_type] : 'Other'),
        id: id,
        start_time: fit_data.activity.sessions[0].start_time.toISOString(),
        total_time: fit_data.activity.total_timer_time,
        distance: fit_data.activity.sessions[0].total_distance,
        points: []
    };

    fit_data.activity.sessions[0].laps.forEach(lap => {
        for (let i = 0; i < lap.records.length; i++) {
            let p = lap.records[i];
            param.points.push({
                time: p.timestamp.toISOString(),
                lat: p.position_lat,
                lon: p.position_long,
                distance: p.distance,
                elevation: ele.get(p.position_lat, p.position_long)
            })
        }
    });

    return tcx.mk(param);
};
