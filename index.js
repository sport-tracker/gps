let gps = module.exports = {
    tcx: require('./tcx'),
    fit: require('./fit'),
    conv: require('./converter')
};
