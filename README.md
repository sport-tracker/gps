# gps

Installation:
```sh
npm i gps
```

Installation using shared gdal library:
```sh
# requires gdal dev library (debian: sudo apt-get install libgdal-dev)
npm i gps --build-from-source --shared_gdal
```
